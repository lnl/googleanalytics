<?php

namespace LocknLoad\GAnalytics;

class Analytics {

    protected $analytics = null;
    protected $profileId = null;

    function __construct() {
        $ghelper = new GAHelper();

        $this->analytics = $ghelper->getService();
        $this->profileId = $ghelper->getFirstprofileId($this->analytics);

    }

    public function getTotalSessoes() {
        // Calls the Core Reporting API and queries for the number of sessions
        // for the last seven days.
        $result = $this->analytics->data_ga->get(
            'ga:' . $this->profileId,
            '7daysAgo',
            'today',
            'ga:sessions');
    
        return $this->printResults($result);

    }

    public function getPvUsrInPeriod($months) {

        $begin = date( "Y-m-d", mktime (0, 0, 0, date("m")-$months, date("d"),  date("Y")) );

        $result = $this->analytics->data_ga->get(
            'ga:' . $this->profileId,
            $begin,
            'yesterday',
            'ga:users,ga:pageviews',
            ['dimensions'=>'ga:nthMonth']);
    
        return $this->printResults($result);

 
    }
    
    public function getPvUsrYesterday() {

        $result = $this->analytics->data_ga->get(
            'ga:' . $this->profileId,
            'yesterday',
            'yesterday',
            'ga:users,ga:pageviews');
    
        return $this->printResults($result);

 
    }

    public function getNewReturnVisitors() {

        $result = $this->analytics->data_ga->get(
            'ga:' . $this->profileId,
            date('Y-m-d', strtotime('today - 30 days')) ,
            date('Y-m-d'),
            'ga:users,ga:newUsers');
    
        return $this->printResults($result);
 
    }

    public function getLocations() {
 
        $result = $this->analytics->data_ga->get(
                'ga:' . $this->profileId,
                date('Y-m-d', strtotime('today - 30 days')) ,
                date('Y-m-d'),
                'ga:users',
                ['dimensions' => 'ga:city','sort'=>'-ga:users']);
   
        return $this->printResults($result);

    }

    function printResults(&$results) {

        return $results->rows;
    
    }


}
