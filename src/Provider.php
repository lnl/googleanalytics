<?php

namespace LocknLoad\GAnalytics;

use Illuminate\Support\ServiceProvider;

class Provider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__. '/GAHelper.php';
        include __DIR__. '/Analytics.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
           
    }
}
